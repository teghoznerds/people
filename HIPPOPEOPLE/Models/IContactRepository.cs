﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HIPPOPEOPLE.Models;

namespace HIPPOPEOPLE.Models
{
    public interface IContactRepository
    {
        void Delete(Contact contact);
        Contact Save(Contact contact);
        IEnumerable<Contact> Contacts { get; }
    }
}