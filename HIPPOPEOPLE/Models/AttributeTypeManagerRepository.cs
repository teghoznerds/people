﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HIPPOPEOPLE.Models
{
    public class AttributeTypeRepository : IAttributeTypeRepository
    {
        private ApplicationDbContext context;

        public AttributeTypeRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<AttributeType> AtrributeType
        {
            get
            {
                return context.AttributeType;
            }
        }

        public void Delete(AttributeType atrributeType)
        {
            context.AttributeType.Remove(atrributeType);
            context.SaveChanges();
        }

        public AttributeType Save(AttributeType atrributeType)
        {
            if (atrributeType.Id == 0)
            {
                context.AttributeType.Add(atrributeType);
            }
            else
            {
                context.Entry(atrributeType).State = EntityState.Modified;
            }

            context.SaveChanges();
            return atrributeType;
        }
    }
}