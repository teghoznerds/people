﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HIPPOPEOPLE.Models;

namespace HIPPOPEOPLE.Models
{
    public interface IContactCardViewModel
    {
        IEnumerable<ContactAtrribute> ContactAtrribute { get; set; }
        IEnumerable<Contact> Contact { get; set; }
    }
}