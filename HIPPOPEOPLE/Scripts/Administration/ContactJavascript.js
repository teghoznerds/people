﻿$(document).ready(function () {

    class ContactJavascript {

        constructor(utils) {
            this.Utilities = utils;
        }

        Init () {
            console.log("Contact Initialized!!");
            var menu = this.Settings;
            this.BindUIAction(menu, this);

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });

            $('.datapicker').datepicker({ format: "dd/mm/yyyy" });

            this.HandleTableData(this, $("#person-table tbody"));

        }
        BindUIAction(menu, contactJavascript) {
            this.HandleEditFunctionality(".edit-person", contactJavascript);
        }

        GetAPerson(contactId, callback, completeCallback) {
            var url = "/Administration/GetAContact";
            var options = {
                url: url,
                data: { contactId: contactId },
                type: "Post",
                dataType: "JSON",
                successCallback: function (data) { callback(data); },
                completeCallback: completeCallback
            };

            this.Utilities.postJson(options);
        }

        GetPersons(callback, completeCallback) {
            var url = "/Administration/GetContacts";
            var options = {
                url: url,
                type: "Post",
                dataType: "JSON",
                successCallback: function (data) { callback(data); },
                completeCallback: completeCallback
            };

            this.Utilities.postJson(options);
        }

        HandleTableData(contactJavascript, table) {
            this.GetPersons(person => {
                var result = JSON.parse(person);
                console.log("result: ", result);

                $(".dataTable-count").html(result.data.length);

                $.each(result.data, (index, element) => {
                    contactJavascript.DisplayTableRows(contactJavascript, table, element);
                });
            });
        }

        HandleEditFunctionality(editBtn, contactJavascript) {
            $(document).on("click", editBtn, function(e) {
                var personId = $(this).data("personid");                
                console.log("personId: ", personId);

                contactJavascript.GetAPerson(personId, aPerson => {
                    var result = JSON.parse(aPerson);
                    contactJavascript.DisplayEditModal(result);
                });
            });
        }

        //display Edit Modal
        DisplayEditModal(person) {
            $(".edit-persons").html(person.FirstName + " " + person.MiddleName + " " + person.LastName + "'s Info");
            $(".Id").val(person.Id);
            $(".FirstName").val(person.FirstName);
            $(".MiddleName").val(person.MiddleName);
            $(".LastName").val(person.LastName);
            $(".EmailAddress").val(person.EmailAddress);
            $(".Phone").val(person.Phone);
            $(".Gravatar").val(person.Gravatar);
            $(".Country").val(person.CountryId);
            $(".DateOfBirth").val(person.DateOfBirth);
            $(".DateCreated").val(person.DateCreated);
            $(".DateLastUpdated").val(person.DateLastUpdated);

            $("#editModal").modal();
        }

        //display table rows
        DisplayTableRows(contactJavascript, table, person) {
            console.log("perons: ", person)
            var string = "<tr>" +
                "<td>"+ person.FirstName +"</td>" +
                "<td>" + person.MiddleName +"</td>" +
                "<td>" + person.LastName +"</td>" +
                "<td>" + person.EmailAddress +"</td>" +
                "<td>" + person.Phone +"</td>" +
                "<td>" + person.Country.CountryName +"</td>" +
                "<td>" + person.DateOfBirth +"</td>" +
                "<td>" + person.DateCreated +"</td>" +
                "<td>" + person.DateLastUpdated +"</td>" +
                "<td>" + contactJavascript.GenerateActions(person.Id) + "</td>" +
                "</tr>";

            table.append(string);
        }

        ///Generate Action Button for table row
        GenerateActions(id) {
            var editButton = "<a class='btn btn-primary edit-person' data-personid='"+ id +"' href='#'><i class='fa fa-edit'></i></a> &nbsp;";
            var deleteButton = "<a class='btn btn-danger' href='/Administration/StronglyTypedContact/DeletePerson/" + id + "'><i class='fa fa-trash'></i></a>";
            var assignButton = "<a class='btn btn-primary' href='/Administration/Contact/AssignContact/" + id + "'><i class='fa fa-hand-lizard-o'></i></a> &nbsp;";;

            var options = assignButton + editButton + deleteButton;
            return options;
        }

    }

    var utils = new Utilities();
    var contactJavascript = new ContactJavascript(utils);
    contactJavascript.Init();
});