﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HIPPOPEOPLE.Models
{
    public class Country
    {
        public int ID { get; set; }
        public string IsO { get; set; }
        public string CountryName { get; set; }
        public string CountryNiceName { get; set; }
        public string IsO3 { get; set; }
        public string NumCode { get; set; }
        public string PhoneCode { get; set; }
    }
    public class Contact
    {
        public Contact()
        {
            DateCreated = DateTime.Now;
            DateLastUpdated = DateTime.Now;
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Gravatar { get; set; }
        public string Phone { get; set; }
        public int CountryId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }
    }
    public class PersonsModel
    {
        public Contact Contact { get; set; }
        public List<ContactAtrribute> ContactAtrribute { get; set; }
    }
    public class ContactCardViewModel : IContactCardViewModel
    {
        public IEnumerable<Contact> Contact { get; set; }

        public IEnumerable<ContactAtrribute> ContactAtrribute { get; set; }
    }
    public class AttributeType
    {
        public AttributeType()
        {
            DateCreated = DateTime.Now;
            DateLastUpdated = DateTime.Now;
        }
        public int Id { get; set; }
        public string Attribute { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
    }
    public class ContactAtrribute
    {
        public ContactAtrribute()
        {
            DateCreated = DateTime.Now;
            DateLastUpdated = DateTime.Now;
        }
        public int Id { get; set; }
        public int ContactId { get; set; }
        public int AttributeId { get; set; }
        public string Value { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }
        [ForeignKey("AttributeId")]
        public virtual AttributeType AttributeType { get; set; }
    }
    public class NewPersonViewModel
    {
        public Contact Contact { get; set; }
        public List<Country> Countries { get; set; }
        public List<AttributeType> AttributeTypes { get; set; }
    }
    public class AssignmentViewModel
    {
        public Contact Contact { get; set; }
        public int Index { get; set; }
        public AttributeType AttributeType { get; set; }
        public List<AttributeType> AttributeTypes { get; set; }
        public virtual List<ContactAtrribute> ContactAtrributes { get; set; }
    }
    public class JSONReturnedType
    {
        public string data { get; set; }
    }

}

