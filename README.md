# Project Title

PEOPLE LIST WITH CRUD FUNCTIONALITY.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things needed to install the Hippo Web App

```
Windows or Any OS with .net implementation, SQL SERVER, IIS
```

### Installing

Copy Deployed Files to [C DRIVE]/[INETPUB]/[WWWROOT]

RUN INETMGR

ADD WEBSITE AND POINT TO THE FOLDER.

## Running the tests

Visual Studio -> Test -> Run All

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [ASP.NET MVC]
* [JAVASCRIPT]
* [MSSQL]


## Authors

* **Aghogho Bernard** - *Initial work* - [infiniteghoz](https://https://bitbucket.org/infiniteghoz/)