﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace HIPPOPEOPLE.Models
{
    public class ContactManagerRepository : IContactRepository
    {
        private ApplicationDbContext context;

        public ContactManagerRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Contact> Contacts
        {
            get
            {
                return context.Contact;
            }
        }

        public void Delete(Contact contact)
        {
            context.Contact.Remove(contact);
            context.SaveChanges();
        }

        public Contact Save(Contact contact)
        {
            if(contact.Id == 0)
            {
                context.Contact.Add(contact);
            }
            else
            {
                context.Entry(contact).State = EntityState.Modified;                
            }

            context.SaveChanges();
            return contact;
        }
    }
}