namespace HIPPOPEOPLE.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HIPPOPEOPLE.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(HIPPOPEOPLE.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var baseDir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin", string.Empty) + "Migrations\\";

            if (context.Country.ToList().Count() == 0)
            {
                //context.CountryTemps.SqlQuery(File.ReadAllText(baseDir + "\\countries.sql"));
                context.Database.ExecuteSqlCommand(File.ReadAllText(baseDir + "\\countries.sql"));
            }

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            string AdminRole = "Admin";

            if (!RoleManager.RoleExists(AdminRole))
            {
                RoleManager.Create(new IdentityRole(AdminRole));
            }

            if (!(context.Users.Any(u => u.UserName == "SUPERUSER")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser
                {
                    UserName = "SUPERUSER",
                    FirstName = "Aghogho",
                    LastName = "Bernard",
                    PhoneNumber = "+230 59291397",
                    Email = "aghoghobernard@gmail.com",
                    IsActive = true
                };
                var result = userManager.Create(userToInsert, "Super@2017");

                if (result.Succeeded)
                {
                    userManager.AddToRole(userToInsert.Id, AdminRole);
                }

            }

        }
    }
}
