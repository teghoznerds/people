﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace HIPPOPEOPLE.Models
{
    public class CountryManagerRepository : ICountryRepository
    {
        private ApplicationDbContext context;

        public CountryManagerRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Country> Countries
        {
            get
            {
                return context.Country;
            }
        }
    }
}