﻿$(document).ready(function () {

    class ContactKendo {

        constructor(utils) {
            this.Utilities = utils;
        }

        //Entry Point
        Init () {
            console.log("Contact Initialized!!");
            var menu = this.Settings;
            this.BindUIAction(menu);

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });

            this.HandleTable($('.persons'), this.Utilities, this);
        }

        //Entry point for UI Actions
        BindUIAction(menu) {
        }

        //Initialise Telerik Kendo Grid
        initTable(element, options, Utilities) {
            Utilities.SetKendoGrid(element, options);
        }

        //Handle Table
        HandleTable(element, utils, contactKendo) {            
            var pageSize = 100;
            console.log("pageSize: ", pageSize);
            var dataSource = new kendo.data.DataSource({
                type: "aspnetmvc-ajax",
                transport: {
                    read: {
                        url: '/Administration/GeTelerikContacts'
                    }
                },
                schema: {
                    model: {
                        fields: {
                            DateOfBirth: { type: "date" },
                            DateCreated: { type: "date" },
                            DateLastUpdated: { type: "date" },
                        }
                    },
                    data: function (response) {
                        var responses = JSON.parse(response);
                        console.log("responses data: ", responses.data);
                        return responses.data.Data;
                    },
                    total: function (response) {
                        var responses = JSON.parse(response);
                        console.log("responses length: ", responses.total);
                        $(".dataTable-count").html(responses.total);
                        return responses.total;
                    }
                },
                                    
                pageSize: pageSize,
                resizable: true,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            });

            var columns = [];
            columns.push({ field: "FirstName", title: "First Name" });
            columns.push({ field: "MiddleName", title: "Middle Name"});
            columns.push({ field: "LastName", title: "Last Name" });
            columns.push({ field: "EmailAddress", title: "Email" });
            columns.push({ field: "Phone", title: "Phone" });
            columns.push({ field: "Country.CountryName", title: "Country"});           
           
            columns.push({ field: "Action", title: "Action", template: contactKendo.tplAction, width: "150px" });

            var options = {
                dataSource: dataSource,
                columns: columns,
                sortable: true,
                filterable: true,
                columnMenu: true,
                navigatable: true,
                resizable: true,
                pageable: {
                    buttonCount: 10,
                    pageSizes: [10, 20, 30, 50, 100, "all"],
                    numeric: true,
                    info: true,
                    refresh: true,
                    input: true
                    //pageSizes: true,
                },
                noRecords: {
                    template: "No data available on current page. Current page is: #=this.dataSource.page()#"
                },
                detailInit: contactKendo.DetailInit,
                dataBound: function () {
                },
                //pageable: true,
                groupable: true,
                editable: "inline"
            };
            contactKendo.initTable(element, options, utils);
        }

        //Manage Child Tables 
        DetailInit(e) {
            console.log("THIS", this);

            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    type: "aspnetmvc-ajax",
                    transport: {
                        read: {
                            url: "/Administration/GeTelerikContacts",
                            data: { contactId: e.data.Id }
                        },
                    },
                    schema: {
                        data: function (response) {
                            var responses = JSON.parse(response);
                            return responses.data.Data;
                        },
                        total: function (response) {
                            var responses = JSON.parse(response);
                            return responses.total;
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 10,
                    //filter: { field: "EmployeeID", operator: "eq", value: e.data.EmployeeID }
                },
                scrollable: false,
                sortable: true,
                resizable: true,
                pageable: true,
                columns: [
                    { field: "DateOfBirth", title: "DOB", template: utils.DateFormat("DateOfBirth"), width: "90px" },
                    { field: "DateCreated", title: "Created On", template: utils.DateFormat("DateCreated"), width: "90px" },
                    { field: "DateLastUpdated", title: "Last Updated", template: utils.DateFormat("DateLastUpdated"), width: "90px" }
                ]
            });

            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    type: "aspnetmvc-ajax",
                    transport: {
                        read: {
                            url: "/Administration/GeTelerikContactAttributes",
                            data: { contactId: e.data.Id }
                        },
                    },
                    schema: {
                        data: function (response) {
                            var responses = JSON.parse(response);
                            return responses.data.Data;
                        },
                        total: function (response) {
                            var responses = JSON.parse(response);
                            return responses.total;
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 10,
                    //filter: { field: "EmployeeID", operator: "eq", value: e.data.EmployeeID }
                },
                scrollable: false,
                sortable: true,
                resizable: true,
                pageable: true,
                columns: [
                    { field: "AttributeType.Attribute", title: "Attribute" },
                    { field: "Value", title: "Value" },
                    { field: "DateCreated", title: "Created On" },
                    { field: "", title: "Action", template: contact.tplChildAction, width: "60px" },
                ]
            });
        }

        //Generate Row Action Buttons
        tplAction(dataItem) {
            var result = '';

            var editUrl = "/Administration" + "/StronglyTypedContact/EditPerson/" + dataItem.Id;

            var edit = '<a href="' + editUrl + '" id="edit" class="btn btn-primary" data-attr="' + dataItem.Id + '">' +
                        '<i class="fa fa-edit"></i>' + '<span class="sr-only">Edit Person</span>' + '</a>&nbsp;';

            var deleteUrl = "/Administration" + "/StronglyTypedContact/DeletePerson/" + dataItem.Id;
            var del = '<a href="' + deleteUrl + '" class="btn btn-danger">' +
                        '<i class="fa fa-trash-o"></i>' + '<span class="sr-only">Delete Person</span>' + '</a>&nbsp;';

            var assignUrl = "/Administration" + "/Contact/AssignContact/" + dataItem.Id;

            var assign = '<a href="' + assignUrl + '" id="edit" class="btn btn-primary" data-attr="' + dataItem.Id + '">' +
                        '<i class="fa fa-hand-lizard-o"></i>' + '<span class="sr-only">Assing Person</span>' + '</a>&nbsp;';

            var str = assign + edit + del;

            result = utils.htmlDecode(str);

            return result;
        }

        tplChildAction(dataItem) {
            console.log("dataItem child", dataItem);

            var result = '';

            var deleteUrl = "/Administration" + "/ContactAttribute/Delete/" + dataItem.Id;
            var del = '<a href="' + deleteUrl + '" class="btn btn-danger">' +
                        '<i class="fa fa-trash-o"></i>' + '<span class="sr-only">Delete Person</span>' + '</a>&nbsp;';

            var str = del;

            result = utils.htmlDecode(str);

            return result;
        }
    }

    var utils = new Utilities();
    var contact = new ContactKendo(utils);

    contact.Init();
});