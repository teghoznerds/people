﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using HIPPOPEOPLE.Controllers;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Threading.Tasks;
using Moq;
using HIPPOPEOPLE.Models;
using NUnit;
using Newtonsoft.Json;
using NUnit.Framework;

namespace HIPPOPEOPLE.Controllers.Tests
{
    [TestFixture]
    public class AdministrationControllerTests
    {
        AdministrationController controller = new AdministrationController();

        [Test]
        public void StronglyTypedContactViewContainsAListOfContact()
        {
            Mock<IContactRepository> contactMock = new Mock<IContactRepository>();

            contactMock.Setup(cm => cm.Contacts).Returns(new Contact[] {
                new Contact { Id = 1, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail.com", FirstName = "Aghogho", LastName ="Bernard", MiddleName ="Ukweduan", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
                new Contact { Id = 2, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail1.com", FirstName = "Deborah", LastName ="Bernard", MiddleName ="Marie", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 3, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail2.com", FirstName = "William", LastName ="Bernard", MiddleName ="Alejandro", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 4, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail3.com", FirstName = "Ejiro", LastName ="Bernard", MiddleName ="Moneyman", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
            });

            AdministrationController controller = new AdministrationController(contactMock.Object);
            var actual = (List<Contact>)controller.StronglyTypedContact().Model;
            Assert.IsInstanceOf<List<Contact>>(actual);
        }

        [Test]
        public void StronglyTypedContactCardViewContainsAnObjectofContactCardViewModel()
        {
            Mock<IContactRepository> contactMock = new Mock<IContactRepository>();

            Mock<IContactAttributeRepository> contactAttributeMock = new Mock<IContactAttributeRepository>();

            contactAttributeMock.Setup(ca => ca.contactAttributes).Returns(new ContactAtrribute[] {
                new ContactAtrribute { Id = 1, AttributeId = 1, ContactId = 1, Value = "Black", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
            });

            contactMock.Setup(cm => cm.Contacts).Returns(new Contact[] {
                new Contact { Id = 1, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail.com", FirstName = "Aghogho", LastName ="Bernard", MiddleName ="Ukweduan", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
                new Contact { Id = 2, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail1.com", FirstName = "Deborah", LastName ="Bernard", MiddleName ="Marie", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 3, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail2.com", FirstName = "William", LastName ="Bernard", MiddleName ="Alejandro", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 4, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail3.com", FirstName = "Ejiro", LastName ="Bernard", MiddleName ="Moneyman", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
            });

            AdministrationController controller = new AdministrationController(contactMock.Object, contactAttributeMock.Object);
            var actual = (ContactCardViewModel)controller.StronglyTypedContactCards().Model;

            Assert.IsInstanceOf<ContactCardViewModel>(actual);
            Assert.AreEqual(4, actual.Contact.Count());
            Assert.AreEqual(1, actual.ContactAtrribute.Count());
        }

        [Test]
        public void JsonResultContactViewShouldReturnAJsonResult()
        {
            Mock<IContactRepository> contactMock = new Mock<IContactRepository>();

            var contactArray = new List<Contact>(){
                new Contact { Id = 1, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail.com", FirstName = "Aghogho", LastName ="Bernard", MiddleName ="Ukweduan", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
                new Contact { Id = 2, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail1.com", FirstName = "Deborah", LastName ="Bernard", MiddleName ="Marie", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 3, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail2.com", FirstName = "William", LastName ="Bernard", MiddleName ="Alejandro", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 4, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail3.com", FirstName = "Ejiro", LastName ="Bernard", MiddleName ="Moneyman", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
            };

            contactMock.Setup(cm => cm.Contacts).Returns(contactArray);

            AdministrationController controller = new AdministrationController(contactMock.Object);
            var actual = controller.GetContacts() as JsonResult;
            dynamic data = actual.Data;

            var deSerialized = JsonConvert.DeserializeObject<dynamic>(data);
            Assert.IsInstanceOf<JsonResult>(actual);
            Assert.AreEqual(4, deSerialized["data"].Count);
            //Assert.IsTrue(actual.);
            Assert.AreEqual("aghoghobernard@gmail.com", (string)deSerialized["data"][0].EmailAddress);
        }

        [Test]
        public void ContactTest()
        {
            ViewResult result = controller.Contact() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void AttributeTypesTest()
        {
            ViewResult result = controller.AttributeTypes() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void ContactTelerikTest()
        {
            Mock<IContactRepository> contactMock = new Mock<IContactRepository>();

            contactMock.Setup(cm => cm.Contacts).Returns(new Contact[] {
                new Contact { Id = 1, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail.com", FirstName = "Aghogho", LastName ="Bernard", MiddleName ="Ukweduan", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
                new Contact { Id = 2, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail1.com", FirstName = "Deborah", LastName ="Bernard", MiddleName ="Marie", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 3, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail2.com", FirstName = "William", LastName ="Bernard", MiddleName ="Alejandro", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 4, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail3.com", FirstName = "Ejiro", LastName ="Bernard", MiddleName ="Moneyman", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
            });

            AdministrationController controller = new AdministrationController(contactMock.Object);
            var actual = (List<Contact>)controller.ContactTelerik().Model;
            Assert.IsInstanceOf<List<Contact>>(actual);
        }

        [Test]
        public void ContactDataTableTest()
        {
            Mock<IContactRepository> contactMock = new Mock<IContactRepository>();

            contactMock.Setup(cm => cm.Contacts).Returns(new Contact[] {
                new Contact { Id = 1, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail.com", FirstName = "Aghogho", LastName ="Bernard", MiddleName ="Ukweduan", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
                new Contact { Id = 2, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail1.com", FirstName = "Deborah", LastName ="Bernard", MiddleName ="Marie", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 3, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail2.com", FirstName = "William", LastName ="Bernard", MiddleName ="Alejandro", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 4, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail3.com", FirstName = "Ejiro", LastName ="Bernard", MiddleName ="Moneyman", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
            });

            AdministrationController controller = new AdministrationController(contactMock.Object);
            var actual = (List<Contact>)controller.ContactDataTable().Model;
            Assert.IsInstanceOf<List<Contact>>(actual);
        }

        [Test]
        public void ContactAngularJsTestShouldReturnAListOfContacts()
        {
            Mock<IContactRepository> contactMock = new Mock<IContactRepository>();

            contactMock.Setup(cm => cm.Contacts).Returns(new Contact[] {
                new Contact { Id = 1, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail.com", FirstName = "Aghogho", LastName ="Bernard", MiddleName ="Ukweduan", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
                new Contact { Id = 2, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail1.com", FirstName = "Deborah", LastName ="Bernard", MiddleName ="Marie", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 3, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail2.com", FirstName = "William", LastName ="Bernard", MiddleName ="Alejandro", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 4, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail3.com", FirstName = "Ejiro", LastName ="Bernard", MiddleName ="Moneyman", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
            });

            AdministrationController controller = new AdministrationController(contactMock.Object);
            var actual = (List<Contact>)controller.ContactAngularJs().Model;
            Assert.IsInstanceOf<List<Contact>>(actual);
        }

        [Test]
        public void ContactJavascriptTestShouldReturnNewPersonViewModelAndACountry()
        {
            Mock<IContactRepository> contactMock = new Mock<IContactRepository>();

            Mock<ICountryRepository> countryMock = new Mock<ICountryRepository>();

            contactMock.Setup(cm => cm.Contacts).Returns(new Contact[] {
                new Contact { Id = 1, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail.com", FirstName = "Aghogho", LastName ="Bernard", MiddleName ="Ukweduan", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now },
                new Contact { Id = 2, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail1.com", FirstName = "Deborah", LastName ="Bernard", MiddleName ="Marie", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 3, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail2.com", FirstName = "William", LastName ="Bernard", MiddleName ="Alejandro", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
                new Contact { Id = 4, CountryId = 1, DateOfBirth = DateTime.Now, EmailAddress = "aghoghobernard@gmail3.com", FirstName = "Ejiro", LastName ="Bernard", MiddleName ="Moneyman", Phone="59291397", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now},
            });

            countryMock.Setup(cm => cm.Countries).Returns(new Country[] {
                new Country { ID = 1, CountryName = "Mauritius", CountryNiceName = "Mauritius", IsO = "XXX", IsO3 ="XXX", NumCode="XXX", PhoneCode="00230"  },
            });

            AdministrationController controller = new AdministrationController(contactMock.Object, countryMock.Object);
            var actual = (NewPersonViewModel)controller.ContactJavascript().Model;
            Assert.IsInstanceOf<NewPersonViewModel>(actual);
            Assert.AreEqual(1, actual.Countries.Count());
        }

        [Test]
        public void AssignContactTest()
        {
            //ViewResult result = controller.AssignContact() as ViewResult;
            //Assert.IsNotNull(result);
        }

        [Test]
        public void AssignContactsTest()
        {
            //ViewResult result = controller.AssignContacts() as ViewResult;
            //Assert.IsNotNull(result);
        }

        [Test]
        public void GetAContactTest()
        {

        }

        [Test]
        public void GetContactsTest()
        {

        }

        [Test]
        public void GetContactsNewTest()
        {

        }

        [Test]
        public void GeTelerikContactsTest()
        {

        }

        [Test]
        public void GeTelerikContactAttributesTest()
        {

        }

        [Test]
        public void NewPersonTest()
        {
            ViewResult result = controller.NewPerson() as ViewResult;
            Assert.IsNotNull(result);
        }

        [Test]
        public void AddAContactTest()
        {
            //ViewResult result = controller.AddAContact() as ViewResult;
            //Assert.IsNotNull(result);
        }

        [Test]
        public void EditPersonTest()
        {
            //ViewResult result = controller.EditPerson() as ViewResult;
            //Assert.IsNotNull(result);
        }

        [Test]
        public void DeletePersonTest()
        {

        }

        [Test]
        public void AttributeTypesTest1()
        {

        }

        [Test]
        public void GetAnAttributeTypeTest()
        {

        }

        [Test]
        public void GetAttributeTypesTest()
        {

        }

        [Test]
        public void DeleteContactAttributeTest()
        {

        }

        [Test]
        public void AddAttributeTypeTest()
        {

        }

        [Test]
        public void DeleteAttributeTypeTest()
        {

        }

        [Test]
        public void NewAttributeTypeTest()
        {
            Mock<ICountryRepository> countryMock = new Mock<ICountryRepository>();

            countryMock.Setup(cm => cm.Countries).Returns(new Country[] {
                new Country { ID = 1, CountryName = "Mauritius", CountryNiceName = "Mauritius", IsO = "XXX", IsO3 ="XXX", NumCode="XXX", PhoneCode="00230"  },
            });

            AdministrationController controller = new AdministrationController(countryMock.Object);
            var actual = (NewPersonViewModel)controller.NewAttributeType().Model;
            Assert.IsInstanceOf<NewPersonViewModel>(actual);
            Assert.AreEqual(1, actual.Countries.Count());
        }

        [Test]
        public void EditAttributeTypeTest()
        {
            Mock<IAttributeTypeRepository> attributeTypeMock = new Mock<IAttributeTypeRepository>();

            attributeTypeMock.Setup(cm => cm.AtrributeType).Returns(new AttributeType[] {
                new AttributeType { Id = 1, Attribute = "Weight", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now,  },
            });

            AdministrationController controller = new AdministrationController(attributeTypeMock.Object);
            var actual = (AttributeType)controller.EditAttributeType(1).Model;
            Assert.IsInstanceOf<AttributeType>(actual);
            Assert.AreEqual("Weight", actual.Attribute);
        }

        [Test]
        public void EditAttributeTypePostAction()
        {
            Mock<IAttributeTypeRepository> attributeTypeMock = new Mock<IAttributeTypeRepository>();

            var attributePost = new AttributeType
            {
                Id = 0,
                Attribute = "Height",
                DateCreated = DateTime.Now,
                DateLastUpdated = DateTime.Now
            };

            var attributePostUpdate = new AttributeType
            {
                Id = 2,
                Attribute = "Weight",
                DateCreated = DateTime.Now,
                DateLastUpdated = DateTime.Now
            };

            attributeTypeMock.Setup(cm => cm.AtrributeType).Returns(new AttributeType[] {
                new AttributeType { Id = 1, Attribute = "Weight", DateCreated = DateTime.Now, DateLastUpdated = DateTime.Now,  },
            });

            AdministrationController controller = new AdministrationController(attributeTypeMock.Object);
            var actual = controller.EditAttributeType(attributePost);
            
            Assert.IsNotNull(actual);
            //Assert.AreEqual(2, actual.);
            //Assert.IsInstanceOf<AttributeType>(actual.Model);
            //Assert.AreEqual(2, actual.);

            var actualIfUpdate = controller.EditAttributeType(attributePostUpdate);
            Assert.IsNotNull(actualIfUpdate);
            Assert.AreEqual(1, attributeTypeMock.Object.AtrributeType.Count());
        }

        [Test]
        public void GetAssignmentItemsTest()
        {

        }

        [Test]
        public void IndexTest()
        {
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}