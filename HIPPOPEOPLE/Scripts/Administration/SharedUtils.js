﻿class Utilities {
    DateFormat(field) {
        return "#= kendo.toString(kendo.parseDate(" + field + ", 'yyyy-MM-dd'), 'dd/MM/yyyy') #";
    }

    SetKendoGrid (Element, Options) {
        //console.log("kendo s: ", kendos);
        var grid = Element.kendoGrid(Options);
        console.log("kendo grid: ", grid);
        return kendo;
    }

    getJson (options) {
        return $.ajax({
            url: options.url,
            type: options.type,
            dataType: options.dataType,
            method: options.method,
            jsonpCallback: options.jsonpCallback,
            success: options.successCallback || renderData,
            error: null,
            timeout: options.timeout
        });
    }

    postJson (options) {
        return $.ajax({
            url: options.url,
            data: options.data,
            method: options.method,
            type: options.type,
            dataType: options.dataType,
            error: null,
            timeout: options.timeout,
            success: options.successCallback || renderData,
            complete: options.completeCallback,
            //done: options.doneCallback
        }); 
    }

    htmlDecode (value) {
        return value.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    }
}