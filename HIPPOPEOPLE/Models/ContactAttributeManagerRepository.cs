﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace HIPPOPEOPLE.Models
{
    public class ContactAttributeManagerRepository : IContactAttributeRepository
    {
        private ApplicationDbContext context;

        public ContactAttributeManagerRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<ContactAtrribute> contactAttributes
        {
            get
            {
                return context.ContactAtrribute;
            }
        }

        public void Delete(ContactAtrribute contactAttribute)
        {
            context.ContactAtrribute.Remove(contactAttribute);
            context.SaveChanges();
        }

        public ContactAtrribute Save(ContactAtrribute contactAttribute)
        {
            if(contactAttribute.Id == 0)
            {
                context.ContactAtrribute.Add(contactAttribute);
            }
            else
            {
                context.Entry(contactAttribute).State = EntityState.Modified;                
            }

            context.SaveChanges();
            return contactAttribute;
        }
    }
}