﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HIPPOPEOPLE.Startup))]
namespace HIPPOPEOPLE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
