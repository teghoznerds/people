﻿$(document).ready(function () {

    class Contact {
        //Entry Point
        Init () {
            console.log("Contact Initialized!!");
            var menu = this.Settings;
            this.BindUIAction(menu);

            this.datatableInit($("#datatable-ajax"), this);
            $('[data-toggle="tooltip"]').tooltip();
            $('.datapicker').datepicker({ format: "dd/mm/yyyy" });
        }

        //Entry point for all UI Actios
        BindUIAction(menu) {

        }

        //Initialise Datatables
        datatableInit(table, contact) {
            var $table = table;

            var details = [];

            var datatable = $table.dataTable({
                "processing": true,
                "serverSide": false,
                "ajax": {
                    "url": $table.data('url'),
                    //"data": { providerId: $table.data('param') },
                    "type": "POST",
                    "dataSrc": function (result) {
                        var result = JSON.parse(result);
                        console.log("the result: ", result);
                        $(".dataTable-count").html(result.data.length);
                        return result.data;
                    }
                },
                "columns": [
                    {
                        "class": "text-center",
                        "orderable": false,
                        "data": null,
                        "defaultContent": '<i data-toggle class="fa fa-plus-square-o text-primary fa-2x" style="cursor: pointer;"></i>',
                    },
                    { data: "Contact.FirstName" },                       
                    { data: "Contact.MiddleName" },
                    { data: "Contact.LastName" },
                    { data: "Contact.EmailAddress" },
                    { data: "Contact.Phone" },
                    { data: "Contact.Country.CountryName" },
                    { data: "Contact.DateOfBirth" },
                    {
                        "class": "text-center",
                        data: function (data) {
                            var value = "";
                            var edit = "<a class='btn btn-primary' href='/Administration/StronglyTypedContact/EditPerson/" + data.Contact.Id + "'><i class='fa fa-edit'></i></a> &nbsp;";
                            var del = "<a class='btn btn-danger' href='/Administration/StronglyTypedContact/DeletePerson/" + data.Contact.Id + "'><i class='fa fa-trash'></i></a>";
                            var assign = '<a class="btn btn-danger" href="/Administration/Contact/AssignContact/' + data.Contact.Id + '" data-toggle="tooltip" title="Assign Attribute"><i class="fa fa-hand-lizard-o"></i></a> &nbsp;';
                            var allBtns = assign + edit + del;

                            return allBtns;
                        },                            
                    }                   
                ],
                "autoWidth": false,
                aoColumnDefs: [
                    {
                        bSortable: false,
                        aTargets: [0],
                        width: "5px"
                    },
                    {
                        bSortable: true,
                        aTargets: [1],
                        width: "150px"
                    },
                    {
                        bSortable: true,
                        aTargets: [2],
                        width: "150px"
                    },
                    {
                        bSortable: true,
                        aTargets: [3],
                        width: "150px"
                    },
                    {
                        bSortable: true,
                        aTargets: [4],
                        width: "100px",
                    },
                    {
                        bSortable: true,
                        aTargets: [5],
                        width: "100px",
                    },
                    {
                        bSortable: true,
                        aTargets: [6],
                        width: "150px",
                    },
                    {
                        bSortable: true,
                        aTargets: [7],
                        width: "150px",
                        type: "date"
                    },
                    {
                        bSortable: true,
                        aTargets: [8],
                        width: "150px",
                        type: "date"
                    }]
            });

            //executable.inserTheRow($table, datatable);
            $(".dropdown-toggle").dropdown();

            $table.on('click', 'i[data-toggle]', function () {
                var $this = $(this),
                    tr = $(this).closest('tr').get(0);

                if (datatable.fnIsOpen(tr)) {
                    $this.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                    datatable.fnClose(tr);
                } else {
                    $this.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                    $(".dropdown-toggle").dropdown();
                    datatable.fnOpen(tr, contact.fnFormatDetails(datatable, tr, contact), 'details');
                    $(".dropdown-toggle").dropdown();
                }
            });

            $('[data-toggle="tooltip"]').tooltip();

        }

        //Manage Child Tables 
        fnFormatDetails (datatable, tr, contact) {
            var data = datatable.fnGetData(tr);
            console.log("data details: ", data);
            return [
                '<table class="table mb-none">',
                    '<tr class="b-top-none">',
                        '<td><label class="mb-none">Created On:</label></td>',
                        '<td>' + data.Contact.DateCreated + '</td>',
                        '<td><label class="mb-none">Last Updated:</label></td>',
                        '<td>' + data.Contact.DateLastUpdated + '</td>',
                    '</tr>',
                '</table><h3>'+ data.Contact.FirstName + " " + data.Contact.LastName +' Attributes</h3>'
            ,
            '<table class="table mb-none">',
                    contact.GenerateChildRows(data.ContactAtrribute, contact),
                '</table>'].join('');

            $('[data-toggle="tooltip"]').tooltip();
        }

        GenerateChildRows(array, contact) {
            var str = "";
            $.each(array, (index, ca) => {
                console.log("ca: " + ca);
                str += '<tr class="b-top-none">'+
                '<td><label class="mb-none">Attribute:</label></td>' +
                '<td>' + ca.AttributeType.Attribute + '</td>' +
                '<td><label class="mb-none">Value:</label></td>' +
                '<td>' + ca.Value + '</td>' +
                '<td>' + contact.GenerateChildRowActions(ca, contact) + '</td>' +
                '</tr>';
            });

            return str;
        }

        GenerateChildRowActions(data, contact) {
            var del = "<a class='btn btn-danger' href='/Administration/ContactAttribute/Delete/" + data.AttributeType.Id + "'><i class='fa fa-trash'></i></a>";
            var allBtns = del;

            return allBtns;         
        }
    }

    var contact = new Contact();
    contact.Init();
});