﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HIPPOPEOPLE.Models;

namespace HIPPOPEOPLE.Models
{
    public interface ICountryRepository
    {
        IEnumerable<Country> Countries { get; }
    }
}