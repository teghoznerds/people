﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace HIPPOPEOPLE.Models
{
    public class Utilities
    {
        public ApplicationUser GetUser(ApplicationUser User)
        {

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Id);

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;
        }

        public ApplicationUser GetUser(IPrincipal User)
        {

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;

        }
    }
}