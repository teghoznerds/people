﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HIPPOPEOPLE.Models;

namespace HIPPOPEOPLE.Models
{
    public interface IAttributeTypeRepository
    {
        void Delete(AttributeType atrributeType);
        AttributeType Save(AttributeType atrributeType);
        IEnumerable<AttributeType> AtrributeType { get; }
    }
}