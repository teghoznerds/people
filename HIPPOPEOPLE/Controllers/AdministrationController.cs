﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HIPPOPEOPLE.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace HIPPOPEOPLE.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdministrationController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        IContactRepository contactManagerRepo;
        IAttributeTypeRepository attributeTypeRepo;
        ICountryRepository countriesManagerRepo;
        IContactAttributeRepository contactAttributesRepo;


        #region CONSTRUCTORS
        public AdministrationController()
        {
            contactManagerRepo = new ContactManagerRepository(db);
            attributeTypeRepo = new AttributeTypeRepository(db);
            countriesManagerRepo = new CountryManagerRepository(db);
            contactAttributesRepo = new ContactAttributeManagerRepository(db);
        }
        public AdministrationController(ContactManagerRepository contactRepo, AttributeTypeRepository attributeRepo, CountryManagerRepository countryRepo, ContactAttributeManagerRepository contactAttributeRepo)
        {
            contactManagerRepo = contactRepo;
            attributeTypeRepo = attributeRepo;
            countriesManagerRepo = countryRepo;
            contactAttributesRepo = contactAttributeRepo;
        }
        public AdministrationController(IContactRepository contactRepo)
        {
            contactManagerRepo = contactRepo;
        }
        public AdministrationController(ICountryRepository countryRepo)
        {
            countriesManagerRepo = countryRepo;
        }
        public AdministrationController(IContactAttributeRepository contactAttrRepo)
        {
            contactAttributesRepo = contactAttrRepo;
        }
        public AdministrationController(IAttributeTypeRepository attribTypeRepo)
        {
            attributeTypeRepo = attribTypeRepo;
        }
        public AdministrationController(IContactRepository contactRepo, IContactAttributeRepository contactAttributeRepo)
        {
            contactManagerRepo = contactRepo;
            contactAttributesRepo = contactAttributeRepo;
        }

        public AdministrationController(IContactRepository contactRepo, ICountryRepository countryRepo)
        {
            contactManagerRepo = contactRepo;
            countriesManagerRepo = countryRepo;
        }

        public AdministrationController(AttributeTypeRepository attributeRepo)
        {
            attributeTypeRepo = attributeRepo;
        }

        public AdministrationController(ContactAttributeManagerRepository contactAttributeRepo)
        {
            contactAttributesRepo = contactAttributeRepo;
        }
        #endregion
        public ActionResult Index()
        {
            return View();
        }

        #region CONTACT
        public ActionResult Contact()
        {
            return View();
        }
        public ViewResult StronglyTypedContact()
        {
            return View(contactManagerRepo.Contacts.ToList());
        }

        public ViewResult StronglyTypedContactCards()
        {
            ContactCardViewModel mode = new ContactCardViewModel();
            mode.Contact = contactManagerRepo.Contacts.ToList();
            mode.ContactAtrribute = contactAttributesRepo.contactAttributes.ToList();
            return View(mode);
        }

        public ViewResult ContactTelerik()
        {
            return View(contactManagerRepo.Contacts.ToList());
        }

        public ViewResult ContactDataTable()
        {
            return View(contactManagerRepo.Contacts.ToList());
        }

        public ViewResult ContactAngularJs()
        {
            return View(contactManagerRepo.Contacts.ToList());
        }
        /// <summary>
        /// Action Result for Javascript
        /// </summary>
        /// <returns></returns>
        public ViewResult ContactJavascript()
        {
            NewPersonViewModel viewModel = new NewPersonViewModel();
            viewModel.Countries = countriesManagerRepo.Countries.ToList();
            return View(viewModel);
        }
        [Route("Administration/Contact/AssignContact/{contactId}")]
        public ViewResult AssignContact(int contactId)
        {
            AssignmentViewModel assignView = new AssignmentViewModel();
            assignView.AttributeTypes = attributeTypeRepo.AtrributeType.ToList();
            assignView.Contact = contactManagerRepo.Contacts.Where(c => c.Id == contactId).FirstOrDefault();
            assignView.ContactAtrributes = contactAttributesRepo.contactAttributes.Where(ca => ca.ContactId == contactId).ToList();
            return View(assignView);
        }      
        [HttpPost]
        public ActionResult AssignContacts(List<ContactAtrribute> ContactAtrribute)
        {
            ContactAtrribute.ForEach(c =>
            {
                if(contactAttributesRepo.contactAttributes.Any(caa => caa.AttributeId == c.AttributeId && caa.ContactId == c.ContactId) == false)
                {
                    var atType = contactAttributesRepo.Save(c);
                }                
            });

            return RedirectToAction("AssignContact", new { contactId = ContactAtrribute[0].ContactId});
        }
        /// <summary>
        /// Method to return A Contacts as JsonResults
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAContact(int contactId)
        {
            var data = contactManagerRepo.Contacts.Where(c => c.Id == contactId).FirstOrDefault();
            var serializedObject = JsonConvert.SerializeObject(data);
            return Json(serializedObject, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Method to return the list of Contacts as  JsonResults
        /// </summary>
        /// <returns></returns>
        public JsonResult GetContacts()
        {
            var data = contactManagerRepo.Contacts.ToList();
            var serializedObject = JsonConvert.SerializeObject(new { data = data});
            return Json(serializedObject, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetContactsNew()
        {
            //var data = contactManagerRepo.Contacts.ToList();
            var allData = contactManagerRepo.Contacts.ToList()
                .Select(ac => new PersonsModel
                {
                    Contact = ac,
                    ContactAtrribute = contactAttributesRepo.contactAttributes.Where(cc => cc.ContactId == ac.Id).ToList()
                });
            
            var serializedObject = JsonConvert.SerializeObject(new { data = allData });
            return Json(serializedObject, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GeTelerikContacts([DataSourceRequest] DataSourceRequest request, int? contactId)
        {
            var forKendo = new { total = contactManagerRepo.Contacts.Count(), data = contactManagerRepo.Contacts.ToDataSourceResult(request) };

            if (contactId.HasValue)
            {
                forKendo = new { total = contactManagerRepo.Contacts.Where(c => c.Id == contactId).Count(), data = contactManagerRepo.Contacts.Where(c => c.Id == contactId).ToDataSourceResult(request) };
            }
            
            var serializedObject = JsonConvert.SerializeObject(forKendo);
            return Json(serializedObject, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GeTelerikContactAttributes([DataSourceRequest] DataSourceRequest request, int contactId)
        {
            var forKendo = new { total = contactAttributesRepo.contactAttributes.Where(ca => ca.ContactId == contactId).Count(), data = contactAttributesRepo.contactAttributes.Where(ca => ca.ContactId == contactId).ToDataSourceResult(request) };
            var serializedObject = JsonConvert.SerializeObject(forKendo);
            return Json(serializedObject, JsonRequestBehavior.AllowGet);
        }
        [Route("Administration/StronglyTypedContact/NewPerson")]
        public ActionResult NewPerson()
        {
            NewPersonViewModel viewModel = new NewPersonViewModel();
            viewModel.Countries = countriesManagerRepo.Countries.ToList();
            return View(viewModel);
        }

        public ActionResult AddAContact(NewPersonViewModel personViewModel)
        {
            var contact = contactManagerRepo.Save(personViewModel.Contact);

            if (contact != null)
            {
                return RedirectToAction("StronglyTypedContact");
            }
            return View();
        }

        [Route("Administration/StronglyTypedContact/EditPerson/{contactId}")]
        public ActionResult EditPerson(int contactId)
        {
            NewPersonViewModel viewModel = new NewPersonViewModel();
            viewModel.Countries = countriesManagerRepo.Countries.ToList();
            viewModel.Contact = contactManagerRepo.Contacts.Where(c => c.Id == contactId).FirstOrDefault();
            return View(viewModel);
        }

        [Route("Administration/StronglyTypedContact/EditPerson")]
        [HttpPost]
        public ActionResult EditPerson(NewPersonViewModel personViewModel)
        {
            var contact = contactManagerRepo.Save(personViewModel.Contact);

            if (contact != null)
            {
                return RedirectToAction("StronglyTypedContact");
            }

            return View(personViewModel);
        }

        [Route("Administration/StronglyTypedContact/DeletePerson/{contactId}")]
        public ActionResult DeletePerson(int contactId)
        {
            var contacToRemove = contactManagerRepo.Contacts.Where(c => c.Id == contactId).FirstOrDefault();
            contactManagerRepo.Delete(contacToRemove);
            return RedirectToAction("StronglyTypedContact");
        }
        #endregion

        #region ATTRIBUTETYPE
        public ActionResult AttributeTypes()
        {
            return View();
        }
        /// <summary>
        /// Method to return An Attribute Type as JsonResults
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAnAttributeType(int attributeTypeId)
        {
            var data = attributeTypeRepo.AtrributeType.Where(a => a.Id == attributeTypeId).FirstOrDefault();
            var serializedObject = JsonConvert.SerializeObject(data);
            return Json(serializedObject, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Method to return the list of Attribute Type as  JsonResults
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAttributeTypes()
        {
            var data = attributeTypeRepo.AtrributeType.ToList();
            var serializedObject = JsonConvert.SerializeObject(new { data = data });
            return Json(serializedObject, JsonRequestBehavior.AllowGet);
        }
        [Route("Administration/ContactAttribute/Delete/{id}")]
        public ActionResult DeleteContactAttribute(int id)
        {
            var contacToRemove = contactAttributesRepo.contactAttributes.Where(c => c.Id == id).FirstOrDefault();
            contactAttributesRepo.Delete(contacToRemove);
            return RedirectToAction("StronglyTypedContact");
        }
        public ActionResult AddAttributeType(AttributeType type)
        {
            var atType = attributeTypeRepo.Save(type);

            if (atType != null)
            {
                return RedirectToAction("AttributeTypes");
            }
            return View();
        }
        [Route("Administration/AttributeType/Delete/{id}")]
        public ActionResult DeleteAttributeType(int id)
        {
            var attType = attributeTypeRepo.AtrributeType.Where(c => c.Id == id).FirstOrDefault();
            attributeTypeRepo.Delete(attType);
            return RedirectToAction("AttributeTypes");
        }
        [Route("Administration/AttributeType/New")]
        public ViewResult NewAttributeType()
        {
            NewPersonViewModel viewModel = new NewPersonViewModel();
            viewModel.Countries = countriesManagerRepo.Countries.ToList();
            return View(viewModel);
        }
        [Route("Administration/AttributeType/Edit/{id}")]
        public ViewResult EditAttributeType(int id)
        {
            var attr = attributeTypeRepo.AtrributeType.Where(ca => ca.Id == id).FirstOrDefault();
            return View(attr);
        }
        [Route("Administration/AttributeType/Edit/{id}")]
        [HttpPost]
        public ActionResult EditAttributeType(AttributeType attributeType)
        {
            var att = attributeTypeRepo.Save(attributeType);

            if (att != null)
            {
                return RedirectToAction("AttributeTypes");
            }

            return View(att);
        }
        /// <summary>
        /// Gets Partial view of Assigment Items
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="indexer"></param>
        /// <returns></returns>
        public ActionResult GetAssignmentItems(int contactId, int indexer)
        {
            AssignmentViewModel assignView = new AssignmentViewModel();
            assignView.AttributeTypes = attributeTypeRepo.AtrributeType.ToList();
            assignView.AttributeType = new AttributeType();
            assignView.Contact = contactManagerRepo.Contacts.Where(c => c.Id == contactId).FirstOrDefault();
            assignView.Index = indexer;
            return PartialView("_AssignmentPartial", assignView);
        }
        #endregion       
    }
}
