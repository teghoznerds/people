﻿$(document).ready(function () {

    class Contact {
        constructor(utils) {
            this.Utilities = utils;
        }
        Init () {
            console.log("Contact Initialized!!");
            var menu = this.Settings;
            this.BindUIAction(menu);
        }
        BindUIAction(menu) {
            this.HandleAddItem(contact, $(".btn-assign-add"), $(".assign-items"));
        }
        HandleAddItem( contact, button, div) {
            button.click((e) => {
                var contactId = $(e.target).data("contactid");
                var formItems = $(".assign-items").find('.form-group').length;
                contact.GetPartialView(contactId, formItems, contact, pc => {
                    contact.DiplayPartialContent(div, pc);
                });
            });
        }

        GetPartialView(contactId, index, contact, callback) {
            var url = "/Administration/GetAssignmentItems";
            var options = {
                url: url,
                type: "Post",
                data: { contactId: contactId, indexer: index },
                dataType: "HTML",
                successCallback: function (data) { callback(data); }
            };

            this.Utilities.postJson(options);
        }

        DiplayPartialContent(Element, content) {
            Element.append(content);
        }

    }

    var utils = new Utilities();
    var contact = new Contact(utils);
    contact.Init();
});