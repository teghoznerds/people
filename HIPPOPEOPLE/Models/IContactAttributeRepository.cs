﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HIPPOPEOPLE.Models;

namespace HIPPOPEOPLE.Models
{
    public interface IContactAttributeRepository
    {
        void Delete(ContactAtrribute contactAttributes);
        ContactAtrribute Save(ContactAtrribute contactAttribute);
        IEnumerable<ContactAtrribute> contactAttributes { get; }
    }
}