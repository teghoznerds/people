$(function () {

    /**
     * Pie charts data and options used in many views
     */

    $("span.pie").peity("pie", {
        fill: ["#f8e805  ", "#edf0f5"]
    })

    $(".line").peity("line",{
        fill: '#f8e805  ',
        stroke:'#edf0f5',
    })

    $(".bar").peity("bar", {
        fill: ["#f8e805  ", "#edf0f5"]
    })

    $(".bar_dashboard").peity("bar", {
        fill: ["#f8e805  ", "#edf0f5"],
    })
});
