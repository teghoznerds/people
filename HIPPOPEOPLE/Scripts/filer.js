﻿var Filer = (function () {
    return {
        Settings: {},
        Init: function () {},
        BindUIAction: function () {
        },
        filer: function (Element) {
            //$('#filer_input2').
            Element.filer({
                changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="fa fa-file-o"></i></div><div class="jFiler-input-text"><h3>Click/Drop on this box</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
                showThumbs: true,
                theme: "dragdropbox",
                templates: {
                    box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                    item: '<li class="jFiler-item">\
                    <div class="jFiler-item-container">\
                        <div class="jFiler-item-inner">\
                            <div class="jFiler-item-thumb">\
                                <div class="jFiler-item-status"></div>\
                                <div class="jFiler-item-info">\
                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 55}}</b></span>\
                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                </div>\
                                {{fi-image}}\
                            </div>\
                            <div class="jFiler-item-assets jFiler-row">\
                                <ul class="list-inline pull-left"></ul>\
                                <ul class="list-inline pull-right">\
                                    <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </li>',
                    itemAppend: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 55}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
                    itemAppendToEnd: false,
                    removeConfirmation: true,
                    _selectors: {
                        list: '.jFiler-items-list',
                        item: '.jFiler-item',
                        remove: '.jFiler-item-trash-action'
                    },
                    files: null,
                    addMore: true,
                    clipBoardPaste: true,
                    excludeName: null,
                    beforeRender: null,
                    afterRender: null,
                    beforeShow: null,
                    beforeSelect: null,
                    onSelect: null,
                    afterShow: null,
                    onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                        var file = file.name;
                        $.post('./php/remove_file.php', { file: file });
                    },
                    onEmpty: null,
                    options: null,
                    captions: {
                        button: "Choose Files",
                        feedback: "Choose files To Upload",
                        feedback2: "files were chosen",
                        drop: "Drop file here to Upload",
                        removeConfirmation: "Are you sure you want to remove this file?",
                        errors: {
                            filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                            filesType: "Only Images are allowed to be uploaded.",
                            filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                            filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                        }
                    },
                    addMore: true
                },
                addMore: true,
                captions: {
                    errors: {
                        //filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                        filesType: "Only Music files are allowed to be uploaded.",
                        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                    }
                }
            });
        }
    }
})();

return Filer;